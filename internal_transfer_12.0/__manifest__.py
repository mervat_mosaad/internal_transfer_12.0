# -*- coding: utf-8 -*-
{
    'name': 'Internal Transfer',
    'version': '1.0',
    'author': 'Mervat Mosaad',
    "sequence": 1,
    'summary': 'Implement Internal Transfer of  Odoo 8 in Odoo 12',
    'category': 'Accounting ',
    'description': """
        Implement Internal Transfer of  Odoo 8 in Odoo 12
    """,
    'website': 'https://www.odoo.com/',
    'depends': ['base', 'account'],
    'data': [
        'security/ir.model.access.csv',
        'views/internal_transfer_view.xml'],

    'installable': True,
    'auto_install': False,
    'application': True,
    }