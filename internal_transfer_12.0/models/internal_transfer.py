from odoo import models, fields, api, _
from odoo.exceptions import ValidationError
import logging
import odoo.addons.decimal_precision as dp
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT as DF, safe_eval

_logger = logging.getLogger(__name__)


class InternalTransfer(models.Model):
    _name = "internal.transfer"
    _description = "internal.transfer"
    _rec_name = 'ref'

    def _get_journal(self):
        return self.env['account.bank.statement'].search([('state', '=', 'open')], limit=1).journal_id.id


    ref = fields.Char(string='Memo', required=True)
    check_num = fields.Char(string='Reference', required=True)
    date = fields.Date(string='Date')
    company_id = fields.Many2one('res.company', string='Company', required=True,
                                 default=lambda self: self.env[
                                     'res.company']._company_default_get(
                                     'account.transfer'))

    source_journal_id = fields.Many2one('account.journal', string='Source',
                                        required=True, ondelete='cascade',
                                        default=_get_journal
                                        )

    target_journal_id = fields.Many2one('account.journal', string='Target',
                                        required=True, ondelete='cascade',
                                        )
    source_move_id = fields.Many2one('account.move', string='Source Move')
    target_move_id = fields.Many2one('account.move', string='Target Move')
    state = fields.Selection([('draft', 'Draft'),
                              ('confirmed', 'Confirmed'),
                              ('cancel', 'Cancel')],
                             string='State', required=True, default='draft')
    amount = fields.Float(string='Amount', required=True,
                          digits=dp.get_precision('Account'))

    @api.one
    @api.constrains('source_journal_id', 'target_journal_id')
    def check_companies(self):
        if (self.source_journal_id.company_id != self.company_id or self.target_journal_id.company_id != self.company_id):
            raise Warning(_('Both Journals must belong to the same company!'))

    @api.multi
    def action_confirm(self):
        self.ensure_one()
        if self.amount <= 0.0:
            raise Warning(_('Amount must be greater than 0!'))
        if not self.date:
            self.date = fields.Date.context_today(self)
        source_move = self.source_move_id.create(self.get_move_vals('source'))
        target_move = self.target_move_id.create(self.get_move_vals('target'))
        bank_cash_object = self.env['account.bank.statement']
        bank_cash_object_line = self.env['account.bank.statement.line']
        source_balance = bank_cash_object.search([('journal_id', '=', self.source_journal_id.id)])._ids
        target_balance = bank_cash_object.search([('journal_id', '=', self.target_journal_id.id)])._ids
        if not source_balance:
            raise ValidationError(
                'You have to define at least 1 intial record for this Source journal ')
        elif not target_balance:
            raise ValidationError('You have to define at least 1 intial record for this Target journal  ')
        else:
            last_target_id = max(target_balance)
            last_source_id = max(source_balance)
            if last_source_id != last_target_id:
                if self.source_journal_id.type == 'bank':
                    for i in bank_cash_object.browse(last_source_id):
                        x = bank_cash_object.create(
                            {'journal_id': i.journal_id.id,
                             'balance_start': i.balance_end_real,
                             'balance_end_real': (
                                 i.balance_end_real - self.amount),
                             'date': i.date,
                             # 'period_id': i.period_id.id,
                             'state': 'open'
                             })

                        _logger.info('Balance End Real Bank')
                        _logger.info(i.balance_end_real)
                        bank_cash_object_line.create({'statement_id': x.id,
                                                      'name': x.id,
                                                      'amount': -self.amount,
                                                      })
                    for i in bank_cash_object.browse(last_target_id):
                        y = bank_cash_object.create(
                            {'journal_id': i.journal_id.id,
                             'balance_start': i.balance_end_real,
                             'balance_end_real': (
                                 i.balance_end_real + self.amount),
                             'date': i.date,
                             'state': 'open',

                             })
                        bank_cash_object_line.create({'statement_id': y.id,
                                                      'name': y.id,
                                                      'amount': self.amount,
                                                      })

                if self.source_journal_id.type == 'cash':
                    _logger.info('In Cash')
                    for i in bank_cash_object.browse(last_source_id):
                        bank_cash_object_line.create({'statement_id': i.id,
                                                      'name': i.id,
                                                      'amount': -self.amount,
                                                      })
                        # By Mervat
                        _logger.info('CASH ID')
                        _logger.info(i.id)
                        i.write({
                            'balance_end_real': (i.balance_end_real - self.amount),
                            'difference': (i.difference + self.amount)
                        })

                        _logger.info('Balance End Real Cash')
                        _logger.info(i.balance_end_real)

                    for i in bank_cash_object.browse(last_target_id):
                        bank_cash_object_line.create({'statement_id': i.id,
                                                      'name': i.id,
                                                      'amount': self.amount,
                                                      })

            elif self.source_journal_id.type == self.target_journal_id.type:
                for i in bank_cash_object.browse(last_source_id):
                    bank_cash_object_line.create({'statement_id': i.id,
                                                  'name': i.id,
                                                  'amount': -self.amount,
                                                  })
                for i in bank_cash_object.browse(last_target_id):
                    bank_cash_object_line.create({'statement_id': i.id,
                                                  'name': i.id,
                                                  'amount': self.amount,
                                                  })

        self.write({
            'target_move_id': target_move.id,
            'source_move_id': source_move.id,
            'state': 'confirmed',
        })

    @api.multi
    def get_move_vals(self, move_type):
        self.ensure_one()

        transfer_account = self.company_id.transfer_account_id
        if not transfer_account:
            raise ValidationError(
                'No transfer account configured in company %s!') % (
                              self.source_journal_id.company_id.name)

        if move_type == 'source':
            ref = _('%s - From' % self.ref)
            journal = self.source_journal_id
            first_account = journal.default_debit_account_id
            second_account = transfer_account
        if move_type == 'target':
            ref = _('%s - To' % self.ref)
            journal = self.target_journal_id
            first_account = transfer_account
            second_account = journal.default_credit_account_id

        name = journal.sequence_id._next()
        move_vals = {
            'ref': ref,
            'name': name,
            'date': self.date,
            'journal_id': journal.id,
            'company_id': self.company_id.id,
        }
        first_line_vals = {
            'name': name,
            'debit': 0.0,
            'credit': self.amount,
            'account_id': first_account.id,
        }
        second_line_vals = {
            'name': name,
            'debit': self.amount,
            'credit': 0.0,
            'account_id': second_account.id,
        }
        move_vals['line_id'] = [
            (0, _, first_line_vals), (0, _, second_line_vals)]
        return move_vals

    @api.multi
    def action_to_draft(self):
        self.write({'state': 'draft'})
        return True

    @api.one
    def action_cancel(self):
        self.source_move_id.unlink()
        self.target_move_id.unlink()
        self.state = 'cancel'



class AccountJournal(models.Model):
    _inherit = "account.journal"

    allow_account_transfer = fields.Boolean(
        string='Allow Account Transfer?',
        default=True,
        help='Set if this journals can be used on account transfers'
    )

    @api.multi
    def open_payments_action(self, payment_type, mode='tree'):
        if payment_type == 'outbound':
            action_ref = 'account.action_account_payments_payable'
        elif payment_type == 'transfer':
            action_ref = 'internal_transfer.internal_transfer_action'
        else:
            action_ref = 'account.action_account_payments'
        [action] = self.env.ref(action_ref).read()
        action['context'] = dict(safe_eval(action.get('context')), default_journal_id=self.id,
                                 search_default_journal_id=self.id)
        if mode == 'form':
            action['views'] = [[False, 'form']]
        return action


class ResCompany(models.Model):
    _inherit = 'res.company'

    transfer_account_id = fields.Many2one(
        'account.account',
        string='Transfer Account',
        help="Account used on transfers between Bank and Cash Journals"
    )
